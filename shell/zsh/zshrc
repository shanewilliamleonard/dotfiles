# If not running interactively, don't do anything
[ -z "$PS1" ] && return

autoload -U colors && colors

# Ignore duplicates in history
setopt histignorealldups sharehistory

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

# Default text editor
export EDITOR='nvim'

# Local PATH settings
if [ -f ~/.pathrc.local ]; then
    source ~/.pathrc.local
fi

# Universal PATH settings
if [ -f ~/.pathrc ]; then
    source ~/.pathrc
fi

# Sanitize duplicate PATH entries
export PATH=`echo $PATH | awk -F: '{for (i=1;i<=NF;i++) { if ( i==1 ) printf("%s",$i); else if ( !x[$i]++ ) printf(":%s",$i); }}'`

# Custom aliases
if [ -f ~/.aliases ]; then
    source ~/.aliases
fi

# Custom local aliases
if [ -f ~/.aliases.local ]; then
    source ~/.aliases.local
fi

# Zsh-specific aliases
function refresh_tmux_env() {
    if [ -n "$TMUX" ]; then
        function refresh_var() {
            tmux_var=$(tmux show-environment | grep "^$1")
            [[ ! -z $tmux_var ]] && export $tmux_var
        }
        refresh_var DISPLAY
        refresh_var SSH_AUTH_SOCK
        refresh_var SSH_CONNECTION
    fi
}
alias refresh='source ~/.zshrc && refresh_tmux_env'

# Custom prompt
if [[ -f ~/.promptrc && ( -z "$SHORT_PROMPT" || $SHORT_PROMPT == "false" || $SHORT_PROMPT == 0 )]]; then
    source ~/.promptrc
elif [ -f ~/.promptrc.short ] && [ $SHORT_PROMPT ]; then
    source ~/.promptrc.short
fi

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2

if whence dircolors >/dev/null; then
  eval "$(dircolors -b)"
  zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
  alias ls='ls --color'
else
  export CLICOLOR=1
  zstyle ':completion:*:default' list-colors ''
fi

zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# From vim-superman: use vim to open man pages
compdef vman="man"

# Use syntax highlighting
if [ -f /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; then
    source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

# iTerm2 shell integration
test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

# iTerm2 Badge variables
iterm2_print_user_vars() {
    iterm2_set_user_var todo "$(cat ~/badge.txt 2> /dev/null)"
}

bindkey -v
bindkey '^R' history-incremental-search-backward

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

# added by travis gem
[ -f ~/.travis/travis.sh ] && source ~/.travis/travis.sh

fpath+="~/.zsh_functions"

# Sanely manage python versions using pyenv.
# See https://opensource.com/article/19/5/python-3-default-mac
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

# Node Version Manager
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Starship command prompt
eval $(starship init zsh)

[ -f ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh ] && source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
[ -f ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] && source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

