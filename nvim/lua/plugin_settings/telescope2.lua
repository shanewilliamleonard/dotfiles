return {
  "nvim-telescope/telescope.nvim",
  event = 'VimEnter',
  branch = '0.1.x',
  dependencies = {
    "nvim-lua/popup.nvim",
    "nvim-lua/plenary.nvim",
    "BurntSushi/ripgrep",
    {
      "nvim-tree/nvim-web-devicons",
      enabled = vim.g.have_nerd_font,
    },
    "nvim-telescope/telescope-project.nvim",
  },
  config = function()
    local builtin = require('telescope.builtin')
    local map = function(key, command, desc)
      vim.keymap.set("n", key, command, { desc = desc })
    end

    map("<leader>sh", builtin.help_tags, "[S]earch [H]elp")
    map("<leader>sk", builtin.keymaps, "[S]earch [K]eymaps")
    map("<leader>sf", function()
        builtin.find_files({hidden = true})
      end, "[S]earch [F]iles")
    map("<leader>sw", builtin.grep_string, "[S]earch current [W]ord")
    map("<leader>sb", builtin.buffers, "[S]earch [B]uffers")
    map("<leader>s/", builtin.live_grep, "[S]earch Live [G]rep")
    map("<leader>sr", builtin.resume, "[S]earch [R]esume")
    map("<leader>sd", builtin.diagnostics, "[S]earch [D]iagnostics")
    map("<leader>sj", builtin.jumplist, "[S]earch [J]umps")
    map("<leader>sq", builtin.quickfix, "[S]earch [Q]uickfix")
    map("<leader>sv", function()
      builtin.find_files({
        cwd = vim.env.HOME .. "/dotfiles/nvim",
      })
    end, "[S]earch Neo[V]im config")

    local telescope = require('telescope')
    telescope.load_extension('project')
    map("<leader>sp", telescope.extensions.project.project, "[S]earch [P]rojects")
  end,
  opts = {
    project = {
      base_dirs = {
        '~/github'
      }
    }
  }
}
