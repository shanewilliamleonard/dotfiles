require("nvim-treesitter.configs").setup({
    ensure_installed = {
        "c", "cpp", "javascript", "typescript", "html", "css", "rust", "python", "bash", "dockerfile", "json", "yaml", 
        "lua", "tsx", "julia", "gn"
    },
    sync_install = false,
    auto_install = true,
    highlight = {
        enable = true,
        additional_vim_regex_highlighting = false,
    }
})
