require('plugin_settings/cmp')
--require('plugin_settings/galaxyline')
require('plugin_settings/lualine')
require('plugin_settings/treesitter')
require('plugin_settings/telescope')

-- TODO: better check than has() because that doesn't work 

-- vim-tmux-navigator
if (vim.fn.has('vim-tmux-navigator') == 1) then
    vim.g.tmux_navigator_disable_when_zoomed = 1
end

-- vim-airline
if (vim.fn.has('vim-airline') == 1) then
    vim.g.airline_powerline_fonts = true
    vim.g['airline#extensions#tabline#enabled'] = false
    vim.g['airline#extensions#tabline#show_buffers'] = true
    vim.g['airline#extensions#branch#enabled'] = true
    vim.g['airline#extensions#tagbar#enabled'] = false
    vim.g['airline#extensions#whitespace#enabled'] = false

    vim.g['airline#extensions#promptline#enabled'] = false
    vim.g['airline#extensions#tmuxline#enabled'] = true
    vim.g['airline#extensions#tmuxline#snapshot_file'] = "~/.tmux.line.conf"
    vim.g['airline#extensions#promptline#snapshot_file'] = "~/.promptrc"
    vim.g['airline#extensions#hunks#non_zero_only'] = false

    vim.g.Powerline_symbols = 'fancy'
end

-- TODO: only enable if lspconfig is installed and current file type is cpp
vim.api.nvim_set_keymap('n', '<leader>ah', ':ClangdSwitchSourceHeader<cr>', { noremap = true })

vim.api.nvim_set_keymap('n', '<leader>ft', ':NvimTreeFindFileToggle<cr>', { noremap = true })
