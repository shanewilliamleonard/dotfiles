function nnoremap(keys, mapping)
    vim.api.nvim_set_keymap('n', keys, mapping, { noremap = true })
end

-- Find files using Telescope command-line sugar.
nnoremap('<leader>ff', '<cmd>Telescope find_files<cr>')
nnoremap('<leader>fg', '<cmd>Telescope live_grep<cr>')
nnoremap('<leader>f/', '<cmd>Telescope git_grep<cr>')
nnoremap('<leader>fb', '<cmd>Telescope buffers<cr>')
nnoremap('<leader>fh', '<cmd>Telescope help_tags<cr>')
nnoremap('<leader>fd', "<cmd>lua require('plugin_settings.telescope').edit_dotfiles()<cr>")
nnoremap('<leader>fr', "<cmd>Telescope resume<cr>")
nnoremap('<leader>fe', "<cmd>Telescope diagnostics<cr>")
nnoremap('<leader>fk', "<cmd>Telescope keymaps<cr>")

nnoremap('<leader>t ', '<cmd>Telescope <cr>')
nnoremap('<leader>t/', '<cmd>Telescope live_grep<cr>')
nnoremap('<leader>tb', '<cmd>Telescope buffers<cr>')
nnoremap('<leader>th', '<cmd>Telescope help_tags<cr>')
nnoremap('<leader>td', "<cmd>lua require('plugin_settings.telescope').edit_dotfiles()<cr>")
nnoremap('<leader>tr', "<cmd>Telescope resume<cr>")
nnoremap('<leader>tt', '<cmd>Telescope resume<cr>')
nnoremap('<leader>tj', "<cmd>Telescope jumplist<cr>")
nnoremap('<leader>tq', "<cmd>Telescope quickfix<cr>")
nnoremap('<leader>te', "<cmd>Telescope diagnostics<cr>")

-- LSP shortcuts
nnoremap('<leader>lr', "<cmd>Telescope lsp_references<cr>")
nnoremap('<leader>ld', "<cmd>Telescope lsp_definitions<cr>")
nnoremap('<leader>lt', "<cmd>Telescope lsp_type_definitions<cr>")
nnoremap('<leader>li', "<cmd>Telescope lsp_implementations<cr>")
nnoremap('<leader>lci', "<cmd>Telescope lsp_incoming_calls<cr>")
nnoremap('<leader>lco', "<cmd>Telescope lsp_outgoing_calls<cr>")
nnoremap('<leader>lws', "<cmd>Telescope lsp_workspace_symbols<cr>")
nnoremap('<leader>ldws', "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>")
nnoremap('<leader>lds', "<cmd>Telescope lsp_document_symbols<cr>")

nnoremap('gs', "<cmd>Telescope git_status<cr>")

-- Module exports
local M = {}

function M.edit_dotfiles()
    require('telescope.builtin').git_files {
        cwd = "~/dotfiles",
        prompt = "~ dotfiles ~"
    }
end

return M
