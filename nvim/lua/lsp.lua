-- Setup for nvim-lspconfig
local opts = { noremap = true, silent = true }
vim.keymap.set("n", "<space>e", vim.diagnostic.open_float, opts)
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, opts)
vim.keymap.set("n", "]d", vim.diagnostic.goto_next, opts)
vim.keymap.set("n", "<space>q", vim.diagnostic.setloclist, opts)

-- Enable completion triggered by <c-x><c-o>
local on_attach = function(client, bufnr)
    vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

    -- Mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local bufopts = { noremap = true, silent = true, buffer = bufnr }
    vim.keymap.set("n", "gD", vim.lsp.buf.declaration, bufopts)
    vim.keymap.set("n", "gd", vim.lsp.buf.definition, bufopts)
    vim.keymap.set("n", "K", vim.lsp.buf.hover, bufopts)
    vim.keymap.set("n", "gi", vim.lsp.buf.implementation, bufopts)
    vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, bufopts)
    vim.keymap.set("n", "<space>wa", vim.lsp.buf.add_workspace_folder, bufopts)
    vim.keymap.set("n", "<space>wr", vim.lsp.buf.remove_workspace_folder, bufopts)
    vim.keymap.set("n", "<space>wl", function()
        print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, bufopts)
    vim.keymap.set("n", "<space>D", vim.lsp.buf.type_definition, bufopts)
    vim.keymap.set("n", "<space>rn", vim.lsp.buf.rename, bufopts)
    vim.keymap.set("n", "<space>ca", vim.lsp.buf.code_action, bufopts)
    vim.keymap.set("n", "gr", vim.lsp.buf.references, bufopts)
    vim.keymap.set("n", "<space>f", function()
        vim.lsp.buf.format({ async = true })
    end, bufopts)
end

-- Completion using nvim-cmp.
local capabilities = require("cmp_nvim_lsp").default_capabilities()
capabilities.offsetEncoding = { "utf-16" } -- Workaround for 'multiple different offset encodings detected for buffer'.

-- Setup for null-ls, which is a wrapper around other language servers.
local null_ls = require("null-ls")
require("null-ls").setup({
    on_attach = on_attach,
    capabilities = capabilities,
    sources = {
        -- null_ls.builtins.formatting.stylua,
        null_ls.builtins.formatting.black,
        -- null_ls.builtins.formatting.shfmt,
        -- null_ls.builtins.formatting.eslint_d,
        null_ls.builtins.formatting.rustfmt,
        null_ls.builtins.formatting.isort,
        null_ls.builtins.formatting.clang_format,
        -- null_ls.builtins.completion.luasnip,
        -- null_ls.builtins.diagnostics.shellcheck,
        -- null_ls.builtins.diagnostics.alex,
        -- null_ls.builtins.diagnostics.buf,
        -- null_ls.builtins.diagnostics.cpplint,
        -- null_ls.builtins.diagnostics.markdownlint,
        null_ls.builtins.diagnostics.mypy,
        -- null_ls.builtins.diagnostics.todo_comments,
        null_ls.builtins.code_actions.gitsigns,
    },
})

-- Setup each language server managed by lspconfig.

-- Configure the simpler language servers in a loop.
local lspconfig = require("lspconfig")
local servers = { "pyright", "bashls", "eslint", "rust_analyzer", "julials" }
for _, server in pairs(servers) do
    lspconfig[server].setup({
        on_attach = on_attach,
        capabilities = capabilities,
    })
end

-- clangd is a bit more complicated, so it's configured separately.
lspconfig["clangd"].setup({
    on_attach = on_attach,
    capabilities = capabilities,
    root_dir = lspconfig.util.root_pattern(".git"),
    cmd = {
        "/home/shane-leonard/github/FlightSystems/.environment/cipd/packages/pigweed/bin/clangd",
        "--compile-commands-dir=/Users/shane/github/FlightSystems/.pw_ide",
        "--query-driver=/Users/shane/github/FlightSystems/.environment/cipd/packages/pigweed/bin/*,/Users/shane/github/FlightSystems/.environment/cipd/packages/arm/bin/*",
        "--background-index",
        "--clang-tidy",
    },
})
