local map = function(mode, keys, mapping, desc)
    vim.keymap.set(mode, keys, mapping, { desc = desc })
end

-- Make everything quicker
map('n', ';', ':', '')

-- Move to beginning/end of line
map('n', 'H', '^', '')
map('n', 'L', '$', '')

-- More natural movement for navigating wrapped lines.
map('n', 'j', 'gj', '')
map('n', 'k', 'gk', '')

-- Quickly save files
map('n', '<leader>w', ':w<cr>', "[W]rite file")
map('n', '<leader>q', ':q<cr>', "[Q]uit")
map('n', '<leader>x', ':x<cr>', "[] Save and quit")

-- Window movement
map('n', '<C-l>', '<C-W>l', '')
map('n', '<C-h>', '<C-W>h', '')
map('n', '<C-j>', '<C-W>j', '')
map('n', '<C-k>', '<C-W>k', '')

-- Exit normal mode
map('i', 'kj', '<esc>', '')

-- Copy-paste
map('i', '<C-v>', '<esc>"*pi', '')
map('v', '<C-c>', '"*yy', '')

-- Quickfix
map('n', '<space>q', ':copen<cr>', '')
