local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable',
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  {
    "folke/which-key.nvim",
    event = 'VimEnter',
    opts = {}
  },

  -- File explorer
  { "kyazdani42/nvim-tree.lua", opts = {} },

  -- AI
  "github/copilot.vim",

  -- Appearance
  { "npxbr/gruvbox.nvim",       dependencies = { "rktjmp/lush.nvim" } },
  "edkolev/tmuxline.vim",
  "nvim-lualine/lualine.nvim",

  -- Basic text editing
  'christoomey/vim-tmux-navigator',
  'axelf4/vim-strip-trailing-whitespace',

  -- IDE and search
  require("plugin_settings/telescope2"),
  require("plugin_settings/cmp2"),
  require("lsp2"),
  {
    'nvim-treesitter/nvim-treesitter',
    build = ':TSUpdate',
    opts = {
      ensure_installed = {
        "c", "cpp", "javascript", "typescript", "html", "css",
        "rust", "python", "bash", "dockerfile", "json",
        "yaml", "lua", "julia", "gn", "vim", "markdown",
        "vimdoc",
      },
      sync_install = false,
      auto_install = true,
      highlight = {
        enable = true,
        additional_vim_regex_highlighting = false,
      }
    }
  },

  -- Version control
  "tpope/vim-fugitive",
  { "lewis6991/gitsigns.nvim", opts = {} },
})
