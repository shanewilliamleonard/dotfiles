-- Windows and buffers
vim.o.hidden = true
vim.o.splitright = true
vim.o.splitbelow = true
vim.o.diffopt = "internal,filler,vertical,closeoff"

-- Wrapping
vim.o.textwidth = 120
vim.o.whichwrap = "h,l,b,s,<,>,[,]"
vim.o.wrap = false

-- Whitespace
vim.o.expandtab = true
vim.o.shiftwidth = 2
vim.o.tabstop = 2
vim.o.softtabstop = 2
vim.o.list = false
vim.o.autoindent = true

-- Search & Replace
vim.o.hlsearch = false
vim.o.incsearch = true
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.inccommand = "split" -- Preview substitution live

-- Ruler and statusline
vim.o.number = true
vim.o.relativenumber = true
vim.o.ruler = true
vim.o.showmode = true
vim.o.showcmd = true
vim.o.laststatus = 2
vim.o.cursorline = true
vim.o.colorcolumn = "120"

-- Encodings
vim.o.fileencoding = "utf-8"
vim.o.fileformat = "unix"
vim.o.fileformats = "unix,mac,dos"

-- Keyboard and mouse
vim.o.mouse = "a"

-- Clipboard
vim.o.clipboard = "unnamed,unnamedplus"

-- Appearance
vim.o.termguicolors = true

-- Completion
vim.o.completeopt = "menuone,noselect" -- Required for nvim-compe

-- Misc
vim.o.autoread = true
vim.o.backspace = "indent,eol,start"
vim.o.showmatch = true
vim.o.scrolloff = 5
vim.o.nrformats = "alpha,hex"

-- Timing
vim.o.updatetime = 250 -- Decrease update time
vim.o.timeoutlen = 300 -- Displays which-key popup

-- TODO: Persistent undo
-- TODO: Swap files
