function map(mode, keys, mapping, desc)
    vim.keymap.set(mode, keys, mapping, desc)
end

return {
  map = map
}
