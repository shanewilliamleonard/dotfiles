-- packer.nvim
--
-- Per https://github.com/wbthomason/packer.nvim#bootstrapping
local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', 'https://github.com/wbthomason/packer.nvim', install_path})
  execute 'packadd packer.nvim'
end

return require('packer').startup(function()
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- Appearance
  use {'npxbr/gruvbox.nvim', requires = { 'rktjmp/lush.nvim' }}
  use 'edkolev/tmuxline.vim'
  --use 'edkolev/promptline.vim'
  --use 'vim-airline/vim-airline'
  --use 'vim-airline/vim-airline-themes'
  use 'whatyouhide/vim-lengthmatters'
  use 'kyazdani42/nvim-web-devicons'
  -- Unfortunately, galaxyline gets extremely slow in certain git contexts.
  --use {
    --'glepnir/galaxyline.nvim',
    --branch = 'main',
    ---- TODO: statusline configuration here
    --config = function() require('plugin_settings/galaxyline') end,
  --}
  use 'nvim-lualine/lualine.nvim'

  -- Basic text editing
  -- use 'tpope/vim-unimpaired'
  -- use 'tpope/vim-surround'
  use 'Scrooloose/nerdcommenter'
  use 'easymotion/vim-easymotion'
  use 'christoomey/vim-tmux-navigator'
  use 'axelf4/vim-strip-trailing-whitespace'

  -- Language-specific highlighting
  --use 'pangloss/vim-javascript'
  --use 'leafgarland/typescript-vim'
  --use 'maxmellon/vim-jsx-pretty'
  --use 'jackguo380/vim-lsp-cxx-highlight'
  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
  -- use { 'willcassella/nvim-gn', run = ':TSUpdate' }
  -- TODO: p00f/nvim-ts-rainbow
  -- TODO: nvim-treesitter/playground

  -- Version control
  use 'tpope/vim-fugitive'
  use {
    'lewis6991/gitsigns.nvim',
    config = function() require('gitsigns').setup() end
  }

  -- Language servers
  use 'neovim/nvim-lspconfig'
  use 'jose-elias-alvarez/null-ls.nvim'
  -- use 'williamboman/nvim-lsp-installer'

  -- Completion
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/cmp-nvim-lsp-signature-help'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-emoji'
  -- TODO: lspsaga

  -- AI
  use 'github/copilot.vim'

  -- Fuzzy finder
  use 'nvim-lua/popup.nvim'
  use 'nvim-lua/plenary.nvim'
  use 'BurntSushi/ripgrep'
  use 'nvim-telescope/telescope.nvim'

  -- Snippets
  use "rafamadriz/friendly-snippets"
  use "kdheepak/cmp-latex-symbols"
  use 'saadparwaiz1/cmp_luasnip'
  use {
      'L3MON4D3/LuaSnip',
      tag = 'v<CurrentMajor>.*',
  }

  -- File explorer
  use {
      'kyazdani42/nvim-tree.lua',
      config = function() require('nvim-tree').setup() end
  }

  use 'godlygeek/tabular'
end)
