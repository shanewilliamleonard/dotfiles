return {
  'neovim/nvim-lspconfig',
  dependencies = {
    { 'williamboman/mason.nvim', opts = {} },
    { 'williamboman/mason-lspconfig.nvim', opts = {} },
    { 'neovim/nvim-lspconfig', opts = {} },
    { 'WhoIsSethDaniel/mason-tool-installer.nvim', opts = {} },

    { "j-hui/fidget.nvim",       opts = {} },
    { "folke/neodev.nvim",       opts = {} },

    {
      "mhartington/formatter.nvim",
      opts = function()
        return {
          logging = true,
          log_level = vim.log.levels.DEBUG,
          filetype = {
            lua = {
              require("formatter.filetypes.lua").stylua,
            },
            python = {
              require("formatter.filetypes.python").black,
            },
            cpp = {
              require("formatter.filetypes.cpp").clang_format,
            },
            json = {
              require("formatter.filetypes.json").prettier,
            },
            ["*"] = {
              require("formatter.filetypes.any").remove_trailing_whitespace,
            },
         }
       }
      end,
      config = function()
        vim.api.nvim_create_autocmd('BufWritePost', {
            group = vim.api.nvim_create_augroup('FormatAutogroup', { clear = true }),
            command = ':FormatWrite',
        })
        vim.keymap.set('n', '<leader>lf', '<cmd>Format<CR>', { desc = "[F]ormat" })
      end
    },
  },
  config = function()
    vim.api.nvim_create_autocmd('LspAttach', {
      group = vim.api.nvim_create_augroup('lsp-attach', { clear = true }),
      callback = function(event)
        local map = function(keys, func, desc)
          vim.keymap.set('n', keys, func, { buffer = event.buf, desc = "[L]SP " .. desc })
        end
        local builtin = require('telescope.builtin')

        map('<leader>lr', builtin.lsp_references, "[R]eferences")
        map('<leader>ld', builtin.lsp_definitions, "[D]efinitions")
        map('<leader>lI', builtin.lsp_implementations, "[I]mplementations")
        map('<leader>lD', builtin.lsp_type_definitions, "Type [D]efinitions")
        map('<leader>ls', builtin.lsp_document_symbols, "Document [S]ymbols")
        map('<leader>lw', builtin.lsp_dynamic_workspace_symbols, "[W]orkspace Symbols")
        map('<leader>ln', vim.lsp.buf.rename, "Re[n]ame")
        map('<leader>lca', vim.lsp.buf.code_action, "[C]ode [A]ction")
        map('<leader>ah', '<cmd>ClangdSwitchSourceHeader<CR>', "[A]lternate [H]eader/Source")
        map('K', vim.lsp.buf.hover, "[H]over")
      end
    })

    local capabilities = vim.lsp.protocol.make_client_capabilities()
    local cmp_capabilities = require('cmp_nvim_lsp').default_capabilities()
    cmp_capabilities.offsetEncoding = "utf-8"
    cmp_capabilities.textDocument.semanticHighlighting = true
    capabilities = vim.tbl_deep_extend('force', capabilities, cmp_capabilities)

    local servers = {
      clangd = {
        root_dir = require("lspconfig").util.root_pattern(".git"),
        cmd = {
          "/home/shane-leonard/github/FlightSystems/.environment/cipd/packages/pigweed/bin/clangd",
          "--compile-commands-dir=/home/shane-leonard/github/FlightSystems/.pw_ide",
          "--query-driver=/home/shane-leonard/github/FlightSystems/.environment/cipd/packages/pigweed/bin/*,/home/shane-leonard/github/FlightSystems/.environment/cipd/packages/arm/bin/*",
          "--background-index",
          "--clang-tidy",
        }
      },
      pyright = {},
      bashls = {},
      rust_analyzer = {},
      julials = {},
      lua_ls = {},
      yamlls = {},
      dockerls = {},
      jsonls = {},
      matlab_ls = {
        cmd = { "matlab-language-server", "--stdio" },
        filetypes = { "matlab" },
        settings = {
          matlab = {
            indexWorkspace = true,
            installPath = "/usr/local/bin/matlab",
          },
        },
        single_file_support = true,
      },
    }

    local ensure_installed = vim.tbl_keys(servers or {})

    require('mason').setup()
    require('mason-lspconfig').setup({
      ensure_installed = ensure_installed,
    })
    for server, config in pairs(servers) do
      config.capabilities = capabilities
      require("lspconfig")[server].setup(config)
    end
  end
}
