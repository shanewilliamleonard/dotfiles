-- Leader key should be set before any plugins are loaded.
vim.g.mapleader = ','
vim.g.maplocalleader = ','

require('plugins2')

-- Base setup
require('keymaps')
require('settings')
require('colors')
-- require('lsp')

-- Plugin setup
-- require('plugin_settings')
