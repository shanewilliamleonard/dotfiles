set :timer, 25*60       # 25-minute pomodoros
set :timer_break, 5*60  # 5-minute breaks
set :warning, 5*60     # Show warning color at <5 minutes (0 to disable)
set :warning_color, 'red,bold' # Warning color for tmux is red/bold
set :break_color, 'blue' # Break color is blue
set :interval, 1 # Refresh timer every second
set :tmux, true # Enable tmux integration
set :tmux_theme, "%.s%s"

# Adds -t --today option, which opens a text file in vim.
option :t, :today, 'open today sheet' do
    `vim -O ~/.thyme-today.md ~/.thyme-records.md < \`tty\` > \`tty\``
end
